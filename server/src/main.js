const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const app = express();
const port = 8080;

const TOKEN_SECRET =
  "2d6201552bfe854287d4732aae08691d1e305db643532edd9faf6f3e1c426d583de4c7991a45b639f193d9cdbfe9d9395dbae8e5a2858e3d5747d4cd01b6b154";

const authenticated = (req, res, next) => {
  const auth_header = req.headers["authorization"];
  const token = auth_header && auth_header.split(" ")[1];
  console.log(token);
  if (!token) return res.sendStatus(401);

  jwt.verify(token, TOKEN_SECRET, (err, info) => {
    if (err) return res.sendStatus(403);
    req.username = info.username;
    next();
  });
};

app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/api/info", authenticated, (req, res) => {
  res.send({ ok: 1 ,username:req.username});
});

app.post("/api/login", bodyParser.json(), async (req, res) => {
  let token = req.body.token;
  let result = await axios.get("https://graph.facebook.com/me", {
    params: {
      fields: "id,name,email",
      access_token: token,
    },
  });
  console.log(result.data);
  if (!result.data.id) {
    res.sendStatus(403);
    return;
  }

  let data = {
    username: result.data.email,
  };
  let access_token = jwt.sign(data, TOKEN_SECRET, { expiresIn: "1800s" });
  res.send({ access_token, username: data.username });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
